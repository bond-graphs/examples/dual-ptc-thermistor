T209 - Dual PTC Thermistor
----
![Bond Graph](bondgraph.png)

# Bond Graph Example
This model is based on an Epcos (now TDK) Dual PTC Thermistor type: T209, from series B59.
The model is purely based on the information provided in the [datasheet](https://datasheet.datasheetarchive.com/originals/library/Datasheet-04/DSA0054241.pdf) and [general information](https://www.tdk-electronics.tdk.com/download/539366/728d0d379187d1dfa0831555b8c93202/pdf-general-technical-information.pdf).
The bondgraph is shown in Figure 1 and part of the simulation result is shown in Figure 2.

![Behavior](plot01.png)

## 20-sim model
The [example](Degaussing_PTC_T209.emx) is a 20-sim model file.
[20-sim](https://www.20sim.com) is a modeling and simulation software package for mechatronic systems.
They have a 14-day free trial available.

## Thermal Bond
The thermal domain in this bond graph operates a bit different from the normal flow and effort pattern.
A [paper](https://doi-org.ezproxy2.utwente.nl/10.1016/0016-0032(75)90131-3) by Jean U. Thoma introduces the different elements used in this example.

# Degaussing of picture tubes
In the days that monitors and TV sets still contained cathode ray tubes regular degaussing was required.
A PTC thermistor was one of the components that was used in an automatic degaussing setup.
Why and how this was done can be read in this [article](https://datasheet.datasheetarchive.com/originals/library/Datasheets-UEA2/DSAFRAZ0021214.pdf) by P. M. Stief.

